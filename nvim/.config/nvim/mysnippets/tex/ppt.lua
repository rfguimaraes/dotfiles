return {
    s({trig="ppt", dscr="Powerpoint-like frame"},
    fmta(
        [[
        \begin{frame}{<>}
            \begin{columns}
                \column{0.5\textwidth}
                \begin{itemize}
                    \item
                \end{itemize}
                \column{0.5\textwidth}
            \end{columns}
        \end{frame}
        ]],
        { i(1) }
    ))
}
