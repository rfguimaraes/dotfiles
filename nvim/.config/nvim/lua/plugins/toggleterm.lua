return {
    'akinsho/toggleterm.nvim',
    version = '*',
    cmd = {
        'ToggleTerm',
        'ToggleTermSendCurrentLine',
        'ToggleTermSendVisualLines',
        'ToggleTermSendVisualSelection',
    },
    config = function()
        require('toggleterm').setup({
            autochdir = true,
            direction = 'vertical',
            auto_scroll = false,
            size = 120
        })
        local Terminal = require('toggleterm.terminal').Terminal

        local git_cz = 'cz c'

        local git_commit = Terminal:new {
            cmd = git_cz,
            dir = 'git_dir',
            hidden = true,
            direction = 'float',
            float_opts = {
                border = 'double'
            }
        }

        function _Git_commit_toggle()
            git_commit:toggle()
        end

        vim.api.nvim_set_keymap('n', '<leader>gg', '<cmd>lua _Git_commit_toggle()<CR>', { noremap = true, silent = true })
    end
}
