return {
    'mfussenegger/nvim-lint',
    lazy = false,
    config = function()
        require('lint').linters_by_ft = {
            tex = { 'chktex', },
            bash = { 'shellcheck' }
            -- python = {'ruff',},
        }

        vim.api.nvim_create_autocmd({ 'BufWritePost' }, {
            callback = function()
                require('lint').try_lint()
            end,
        })

        local chktex = require('lint.linters.chktex')
        local chktexrc_path = os.getenv('HOME') .. '/chktexrc.local'

        chktex.args = {
            '-v0', '-I0', '-s', ':', '-f', '%l%b%c%b%d%b%k%b%n%b%m%b%b%b', '-l', chktexrc_path
        }
    end
}
