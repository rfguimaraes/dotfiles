-- Fuzzy Finder
return {
    'nvim-telescope/telescope.nvim',
    tag = '0.1.6', -- or branch = '0.1.x',
    cmd = 'Telescope',
    ft = 'mason',
    config = function()
        local actions = require('telescope.actions')
        local trouble = require('trouble.sources.telescope')

        require('telescope').setup({
            defaults = {
                mappings = {
                    i = {
                        ['<M-t>'] = trouble.open,
                        ['<C-k>'] = require("telescope-live-grep-args.actions").quote_prompt(),
                    },
                    n = { ['<M-t>'] = trouble.open },
                }
            },
            extensions = {
                fzf = { fuzzy = true, override_file_sorter = true, case_mode = 'smart_case' },
                ['ui-select'] = { require('telescope.themes').get_dropdown() },
            },
            vimgrep_arguments = { 'rg', '--smart-case' },
        })
        require('telescope').load_extension('fzf')
        require('telescope').load_extension('ui-select')
        require('telescope').load_extension('dap')
        require('telescope').load_extension('conventional_commits')
        require('telescope').load_extension('refactoring')
        require('telescope').load_extension('live_grep_args')
        require('telescope').load_extension('git_worktree')
    end,
    dependencies = {
        { 'folke/trouble.nvim' },
        { 'nvim-lua/plenary.nvim' },
        { 'BurntSushi/ripgrep' },
        { 'olacin/telescope-cc.nvim' },
        { 'sharkdp/fd' },
        { 'ThePrimeagen/refactoring.nvim' },
        {
            'nvim-telescope/telescope-fzf-native.nvim',
            build =
            'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build'
        },
        { 'nvim-telescope/telescope-ui-select.nvim' },
        {
            'nvim-telescope/telescope-dap.nvim',
            dependencies = {
                'mfussenegger/nvim-dap',
                { 'nvim-treesitter/nvim-treesitter', build = ':TSUpdate' }
            }
        },
        { 'nvim-telescope/telescope-live-grep-args.nvim' },

    },
    keys = {
        {
            '<leader>ff',
            function() require('telescope.builtin').find_files({ hidden = true }) end,
            desc =
            'Find files (root)'
        },
        {
            '<leader>fF',
            function()
                require('telescope.builtin').find_files({
                    cwd = require('telescope.utils').buffer_dir() })
            end,
            desc =
            'Find files (cwd)'
        },
        {
            '<leader>fb',
            function() require('telescope.builtin').buffers() end,
            desc =
            'Find buffers'
        },
        {
            '<leader>fg',
            function() require('telescope').extensions.live_grep_args.live_grep_args() end,
            desc =
            'File search by live grep'
        },
        {
            '<leader>ft',
            function() require('telescope.builtin').git_files() end,
            desc =
            'File search (.git)'
        },
        {
            '<leader>fs',
            function() require('telescope.builtin').grep_string({ search = vim.fn.input('Grep > ') }) end,
            desc =
            'Grep string'
        },
        {
            '<leader>rr',
            function() require('telescope').extensions.refactoring.refactors() end,
            desc = 'Refactoring',
            mode = { 'n', 'x' }
        },
        {
            '<leader>Wc',
            function() require('telescope').extensions.git_worktree.create_git_worktree() end,
            desc = 'Create worktree',
            mode = { 'n', 'x' }
        },
        {
            '<leader>Wt',
            function() require('telescope').extensions.git_worktree.git_worktree() end,
            desc = 'Switch or delete worktree',
            mode = { 'n', 'x' }
        },
    }
}
