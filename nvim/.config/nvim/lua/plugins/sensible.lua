-- Sensible options (currently I generally agree with them)
return {
    'tpope/vim-sensible',
    lazy = false,
    config = function ()
        vim.opt.listchars = {
            eol = '⤶',
            tab = '>·',
            space = '␣',
            extends = '◀',
            precedes = '▶',
        }

        print("WARN: workaround to listchars enabled")

    end
}
