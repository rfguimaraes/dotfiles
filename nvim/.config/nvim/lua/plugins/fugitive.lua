return {
    'tpope/vim-fugitive',
    lazy = true,
    cmd = {'Git', 'Gvdiffsplit', 'Gdiffsplit'}
}
