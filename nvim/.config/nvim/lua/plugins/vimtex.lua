return {
    'lervag/vimtex',
    ft = 'tex',
    config = function()
        local g = vim.g

        g.tex_flavor = 'latex'

        g.vimtex_view_method = 'zathura'
        g.vimtex_compiler_method = 'latexmk'
        g.vimtex_quickfix_mode = 0
        -- TODO: convert this setting to the Lua format
        vim.cmd([[
                    let g:vimtex_env_toggle_math_map = {
                        \ '$': '\(',
                        \ '\(': 'align*',
                        \ '$$': 'align*',
                        \ '\[': 'align*',
                        \ 'align*': '\('
                    \}
                ]])
    end
}
