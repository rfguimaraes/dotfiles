-- Cool status line
return {
    'nvim-lualine/lualine.nvim',
    event = 'VeryLazy',
    config = function()
        local navic = require('nvim-navic')
        require('lualine').setup({
            sections = {
                lualine_c = {
                    {
                        function()
                            return navic.get_location()
                        end,
                        cond = function()
                            return navic.is_available()
                        end
                    },
                }
            }
        })
    end,
    dependencies = {
        { 'nvim-tree/nvim-web-devicons', lazy = true },
        { 'SmiteshP/nvim-navic' }
    }
}
