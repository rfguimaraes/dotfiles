return {
    'kevinhwang91/nvim-ufo',
    dependencies = {
        'kevinhwang91/promise-async'
    },
    lazy = false,
    config = true,
    keys = {
        { 'zR', require('ufo').openAllFolds, desc = 'Open All Folds'},
        { 'zM', require('ufo').closeAllFolds, desc = 'Close All Folds'},
    }
}
