return {
    'folke/trouble.nvim',
    cmd = { 'Trouble', 'TroubleClose', 'TroubleToggle', 'TroubleRefresh' },
    keys = {
        { '<leader>xx', '<cmd>TroubleToggle<CR>', desc = 'Open trouble' },
        { '<leader>xw', function() require('trouble').open('workspace_diagnostics') end, desc = 'Workspace diag.' },
        { '<leader>xd', function() require('trouble').open('document_diagnostics') end, desc = 'Document diag.' },
        { '<leader>xq', function() require('trouble').open('quickfix') end,  desc = 'Trouble quickfix list' },
        { '<leader>xl', function() require('trouble').open('loclist') end,  desc = 'Trouble locations list' },
        { '<leader>gR', function() require('trouble').open('lsp_references') end, desc = 'Old gR: enter virtual replace mode' },
        { 'gR', function() require('trouble').open('lsp_references') end, desc = 'Trouble references' },
    },
    dependencies = { 'nvim-tree/nvim-web-devicons' },
    opts = {
        -- your configuration comes here
        -- or leave it empty to use the default settings
        -- refer to the configuration section below
    },
}
