return {
    'mfussenegger/nvim-dap-python',
    ft = 'python',
    config = function()
        require('dap-python').setup('~/.local/share/nvim/mason/packages/debugpy/venv/bin/python')
        table.insert(require('dap').configurations.python, {
            type = 'python',
            request = 'launch',
            name = 'My custom launch configuration',
            program = '${file}',
            justMyCode = false,
            -- ... more options, see https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings
        })
        table.insert(require('dap').configurations.python, {
            type = 'python',
            request = 'launch',
            name = 'Run pulp manager 3',
            program = '/home/rfguimaraes/Documents/<repo>/<worktree>/path/to/script.py',
            justMyCode = false,
            cwd = '/home/rfguimaraes/Documents/<repo>/<worktree>/path/to/',
            args = { "--some-arg", "another_arg" },
            -- ... more options, see https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings
        })
        table.insert(require('dap').configurations.python, {
            type = 'python',
            request = 'launch',
            name = 'X-Launch file with arguments',
            program = '${file}',
            cwd = '/home/rfguimaraes/Documents/<repo>/<worktree>/path/to',
            args = function()
                local args_string = vim.fn.input('Arguments: ')
                return vim.split(args_string, " +")
            end,
            justMyCode = false,
        })
        require('dap-python').test_runner = 'pytest'
    end,
    keys = {
        { '<leader>dn', function() require('dap-python').test_method() end, desc = 'Test method' },
        { '<leader>df', function() require('dap-python').test_class() end,  desc = 'Test class' },
        {
            '<leader>dS',
            function() require('dap-python').debug_selection({}) end,
            mode = 'v',
            desc =
            'Debug selection'
        }
    }
}
