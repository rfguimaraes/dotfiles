-- LSP Support
return {
    {
        'williamboman/mason.nvim',
        cmd = { 'Mason', 'MasonInstall', 'MasonUpdate' },
        lazy = true,
        config = true
    },
    {
        'williamboman/mason-lspconfig.nvim',
        dependencies = {
            { 'hrsh7th/cmp-nvim-lsp' },
        }
    },
    {
        'VonHeikemen/lsp-zero.nvim',
        branch = 'v4.x',
        lazy = true,
        config = false,
        init = function()
            -- Disable automatic setup, we are doing it manually
            vim.g.lsp_zero_extend_cmp = 0
            vim.g.lsp_zero_extend_lspconfig = 0
        end,
    },
    {
        'neovim/nvim-lspconfig',
        cmd = { 'LspInfo', 'LspInstall', 'LspStart' },
        event = { 'BufReadPre', 'BufNewFile' },
        dependencies = {
            { 'hrsh7th/cmp-nvim-lsp' },
            { 'williamboman/mason-lspconfig.nvim' },
            { 'simrat39/rust-tools.nvim' },
            { 'kevinhwang91/nvim-ufo' },
            {
                'SmiteshP/nvim-navbuddy',
                dependencies = {
                    'SmiteshP/nvim-navic',
                    'MunifTanjim/nui.nvim',
                    'simrat39/inlay-hints.nvim',
                },
            },
            { 'nvimdev/lspsaga.nvim' },
        },
        config = function()
            local lsp_zero = require('lsp-zero')

            local lsp_on_attach = function(client, bufnr)
                if client.server_capabilities.documentSymbolProvider then
                    require('nvim-navic').attach(client, bufnr)
                    require('nvim-navbuddy').attach(client, bufnr)
                end
                -- TODO: Swap original behaviour with these without leader, since I will use these more
                -- that is, same strategy as swapping \ and ,
                vim.keymap.set('n', '<leader>H', '<cmd>lua vim.lsp.buf.hover()<cr>',
                    { buffer = bufnr, desc = 'Hover' })

                vim.keymap.set('n', '<leader>gd', 'gd', { buffer = bufnr, desc = 'Old gd: To declaration (C)' })
                vim.keymap.set('n', 'gd', '<cmd>Telescope lsp_definitions<cr>',
                    { buffer = bufnr, desc = 'Definitions' })

                vim.keymap.set('n', '<leader>gD', 'gD', { buffer = bufnr, desc = 'Old gD: To global declaration (C)' })
                vim.keymap.set('n', '<leader>gD', '<cmd>lua vim.lsp.buf.declaration()<cr>',
                    { buffer = bufnr, desc = 'Declaration' })

                vim.keymap.set('n', '<leader>gi', 'gi',
                    { buffer = bufnr, desc = 'Old gi: Move to the last insertion and INSERT' })
                vim.keymap.set('n', 'gi', '<cmd>Telescope lsp_implementations<cr>',
                    { buffer = bufnr, desc = 'Implementations' })

                vim.keymap.set('n', '<leader>go', 'go', { buffer = bufnr, desc = 'Old go: Go to byte' })
                vim.keymap.set('n', '<leader>go', '<cmd>Telescope lsp_type_definitions<cr>',
                    { buffer = bufnr, desc = 'Type definitions' })

                vim.keymap.set('n', '<leader>gr', 'gr', { buffer = bufnr, desc = 'Old gr: replace virtual chars' })
                vim.keymap.set('n', 'gr', '<cmd>Telescope lsp_references<cr>',
                    { buffer = bufnr, desc = 'References' })

                vim.keymap.set('n', '<leader>gs', 'Old gs: Sleep', { buffer = bufnr, desc = 'Signature help' })
                vim.keymap.set('n', 'gs', '<cmd>lua vim.lsp.buf.signature_help()<cr>',
                    { buffer = bufnr, desc = 'Signature help' })

                vim.keymap.set('n', '<F2>', '<cmd>lua vim.lsp.buf.rename()<cr>', { buffer = bufnr, desc = 'Rename' })
                vim.keymap.set({ 'n', 'x' }, '<F3>', '<cmd>lua vim.lsp.buf.format({async = true})<cr>',
                    { buffer = bufnr, desc = 'Format' })
                vim.keymap.set('n', '<F4>', '<cmd>lua vim.lsp.buf.code_action()<cr>',
                    { buffer = bufnr, desc = 'Code action' })

                vim.keymap.set('n', '<leader>gl', '<cmd>lua vim.diagnostic.open_float()<cr>',
                    { buffer = bufnr, desc = 'Open float' })

                vim.keymap.set('n', '<leader>[d', 'Previous diag.',
                    { buffer = bufnr, desc = 'Old [d: Show first #define' })
                vim.keymap.set('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<cr>',
                    { buffer = bufnr, desc = 'Previous diag.' })

                vim.keymap.set('n', '<leader>]d', ']d',
                    { buffer = bufnr, desc = 'Old ]d: Show first #define from cursor' })
                vim.keymap.set('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<cr>',
                    { buffer = bufnr, desc = 'Next diag.' })
            end


            lsp_zero.extend_lspconfig({
                capabilities = require('cmp_nvim_lsp').default_capabilities(),
                lsp_attach = lsp_on_attach,
                float_boarder = 'rounded',
                sign_text = true,
            })

            -- lsp_zero.set_server_config({
            --     capabilities = {
            --         textDocument = {
            --             foldingRange = {
            --                 dynamicRegistration = false,
            --                 lineFoldingOnly = true
            --             }
            --         }
            --     }
            -- })

            local ih = require('inlay-hints')
            require('lspconfig').lua_ls.setup({
                on_attach = function(client, bufnr)
                    ih.on_attach(client, bufnr)
                end,
                settings = {
                    Lua = {
                        hint = {
                            enable = true,
                        },
                    },
                },
            })

            require('mason')
            require('mason-lspconfig').setup({
                ensure_installed = {
                    'texlab',
                    'rust_analyzer',
                    'lua_ls',
                    'ruff_lsp',
                    'jdtls',
                },
                handlers = {
                    function(server_name)
                        require('lspconfig')[server_name].setup({})
                    end,
                    jdtls = lsp_zero.noop,
                    lua_ls = function()
                        require('lspconfig').lua_ls.setup(lsp_zero.nvim_lua_ls())
                    end,
                    ruff_lsp = function()
                        require('lspconfig').ruff_lsp.setup({
                            init_options = {
                                settings = {
                                    -- Any extra CLI arguments for `ruff` go here.
                                    args = {
                                        '--select=ANN,ARG,A,B,BLE,C4,COM,D,DTZ,E,ERA,EXE,EM,F,G,FBT,ICN,INP,INT,ISC,N,PGH,PIE,PL,PLE,PLR,PLW,PT,PTH,PYI,Q,RET,RSE,RUF,S,SLF,TCH,TID,TRY,UP,SIM,W,YTT',
                                        '--ignore=D212,D213'
                                    },

                                }
                            }
                        })
                    end,
                    rust_analyzer = function()
                        local path = vim.fn.glob(vim.fn.stdpath "data" .. "/mason/packages/codelldb/extension/")

                        local codelldb_path = path .. "adapter/codelldb"
                        local liblldb_path = path .. "lldb/lib/liblldb.so"
                        local rust_tools = require('rust-tools')
                        -- local extension_path = vim.env.HOME .. '.vscode/extensions/vadimcn.vscode-lldb-1.10.0/'
                        -- local codelldb_path = extension_path .. 'adapter/codelldb'
                        -- local liblldb_path = extension_path .. '/lldb/lib/liblldb.so'
                        rust_tools.setup({
                            server = {
                                on_attach = function(_, bufnr)
                                    vim.keymap.set('n', '<leader>ca', rust_tools.hover_actions.hover_actions,
                                        { buffer = bufnr, desc = "Hover actions" })
                                end
                            },
                            dap = {
                                adapter = require("rust-tools.dap").get_codelldb_adapter(
                                    codelldb_path, liblldb_path
                                )
                            }
                        })
                    end,
                },
            })
        end
    }
}
