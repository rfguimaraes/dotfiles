vim.g.mapleader = ' '


-- vim.keymap.set('n', '<leader>pv', vim.cmd.Ex, { desc = 'netrw' })
vim.keymap.set('n', '<leader>tp', vim.cmd.tabp, { desc = 'Next tab' })
vim.keymap.set('n', '<leader>tn', vim.cmd.tabn, { desc = 'Previous tab' })
vim.keymap.set('n', '<leader>,', vim.cmd.noh, { desc = 'Clear highlights' })

-- Adapted from ThePrimeagen's video: 0 to LSP

-- Move lines around when visually highlighted and with automatic indentation
vim.keymap.set('v', 'J', ":m '>+1<CR>gv=gv", { desc = "Mode highlighted down" })
vim.keymap.set('v', 'K', ":m '<-2<CR>gv=gv", { desc = "Mode highlighted up" })

-- Keep cursor in place when joining lines
vim.keymap.set('n', 'J', 'mzJ`z', { desc = "Join with cursor fixed" })

-- Keep cursor in the middle of the screen during half jumps
vim.keymap.set('n', '<C-d>', '<C-d>zz', { desc = 'Half-jump down (centered)' })
vim.keymap.set('n', '<C-u>', '<C-u>zz', { desc = 'Half-jump up (centered)' })
-- Keep search results in the middle of the screen
vim.keymap.set('n', 'n', 'nzzzv', { desc = 'Search forward (centered)' })
vim.keymap.set('n', 'N', 'Nzzzv', { desc = 'Search backwards (centered)' })

-- Visual selection paste (replace) without overriding the paste register
-- Essentially uses the void register
vim.keymap.set('x', '<leader>p', '"_dP', { desc = 'Clean paste-replace' })
-- More uses of the void register
vim.keymap.set('n', '<leader>c', '"_d', { desc = 'Clean delete' })
vim.keymap.set('v', '<leader>c', '"_d', { desc = 'Clean delete' })

-- Easier access to the system's clipboard
vim.keymap.set('n', '<leader>y', '"+y', { desc = 'Copy to clipboard' })
vim.keymap.set('v', '<leader>y', '"+y', { desc = 'Copy to clipboard' })
vim.keymap.set('n', '<leader>Y', '"+Y', { desc = 'Copy (line) to clipboard' })

-- Quickfix navigation
vim.keymap.set('n', '<leader>qk', '<cmd>cnext<CR>zz', { desc = 'Next error' })
vim.keymap.set('n', '<leader>qj', '<cmd>cprev<CR>zz', { desc = 'Previous error' })
vim.keymap.set('n', '<leader>qh', '<cmd>lprev<CR>zz', { desc = 'Next location' })
vim.keymap.set('n', '<leader>ql', '<cmd>lnext<CR>zz', { desc = 'Previous location' })
vim.keymap.set('n', '<leader>qq', function() vim.diagnostic.set_loclist() end, { desc = 'Set loclist' })

-- Easy replace word under cursor
vim.keymap.set('n', '<leader>s', ':%s/\\<<C-r><C-w>\\>/<C-r><C-w>/gI<Left><Left><Left>',
    { desc = 'Replace word under cursor' })

-- Formatting
vim.keymap.set('n', '<leader>fc', function() vim.lsp.buf.format() end, { desc = 'Format code' })

vim.keymap.set('n', '<leader>wa', function() vim.lsp.buf.add_workspace_folder() end, { desc = 'Add workspace folder' })
vim.keymap.set('n', '<leader>wr', function() vim.lsp.buf.remove_workspace_folder() end,
    { desc = 'Remove workspace folder' })
vim.keymap.set('n', '<leader>wl', function() print(vim.inspect(vim.lsp.buf.list_workspace_folders())) end,
    { desc = 'List workspace folders' })
