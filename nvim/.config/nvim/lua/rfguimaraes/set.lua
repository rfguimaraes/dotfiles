local o = vim.opt
local g = vim.g

g.mapleader = ','

-- Fat cursor
-- o.guicursor = ''

-- Enable hybrid line numbering
-- Show line numbers
o.nu = true
-- Relative line numbers
o.relativenumber = true

-- Indentation
-- A hard Tab will display as 4 spaces
o.tabstop = 4
-- Insert/delete 4 spaces when hitting a Tab/Backspace
o.softtabstop = 4
-- Use spaces instead of tabs
o.expandtab = true
-- Operation >> indents 4 columns; << dedents 4 columns
o.shiftwidth = 4
-- Round indent to multiple of 'shiftwidth'
o.shiftround = true
-- Smart indent
o.autoindent = true
o.smartindent = true
-- Backspace behaviour
o.backspace = 'indent,eol,start'
-- Long lines
-- Wrap long lines
o.wrap = true
-- Color column 80
o.colorcolumn = '80'

-- Preferred splitting
o.splitbelow = true
o.splitright = true

-- Find and replace
-- Highlight search results
o.hls = true
-- Search and highlight while typing
o.incsearch = true
-- Match all in line in replace, not only the first
o.gdefault = true

-- Swap and undo files
local tmpdir = os.getenv('HOME') .. '/.vim/tmp'
o.swapfile = true
o.directory = { tmpdir }
o.backup = true
o.backupdir = { tmpdir }
o.undofile = true
o.undodir = { tmpdir }

-- Parenthesis
-- Show briefly who matches the new one
o.showmatch = true

-- Other cosmetics
-- Enable better colours
o.termguicolors = true
-- Allocate space for the symbols on the leftmost column
o.signcolumn = 'yes'
-- Show cursor coordinates
o.ruler = true

-- Formatting and spelling
o.formatoptions='n1jcroqlj'
o.spelllang='en_gb'
o.spell = true

-- Do not conceal
o.conceallevel = 0

-- Internal encoding (e.g. buffers, registers)
o.encoding = 'utf-8'
-- Minimal number of screen lines to keep above and below the cursor
o.scrolloff = 4
-- 300ms of no cursor movement to trigger CursorHold
o.updatetime = 300
-- We often use fast terminals
o.ttyfast = true

-- This setting is by vim-sensible.
-- Therefore, I am setting it in after/plugin/sensible.lua
-- It is here only for documentation purposes.
o.listchars = {
    eol = '⤶',
    tab = '>·',
    space = '␣',
    extends = '◀',
    precedes = '▶',
}

-- Settings related to nvim-ufo
o.foldcolumn = '1'
o.foldlevel = 99
o.foldlevelstart = 99
vim.o.foldenable = true

g.python3_host_prog = '/home/rfguimaraes/.pyenv/versions/nvim/bin/python'

-- TODO: update to improve lua autocommands API
vim.cmd([[
  augroup MyLatexAutocmds
    autocmd!
    autocmd FileType tex setlocal iskeyword-=:
  augroup END
]])
