vim.g.git_worktree_log_level = 'trace'
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)


-- Map backslash (\) to the original function of comma (,)
-- vim.keymap.set({ 'n', 'x', 'o' }, '<Bslash>', ',', { desc = 'Old comma' })

vim.g.mapleader = ' '
vim.g.maplocalleader = '\\'

return require('lazy').setup('plugins')
