# Dotfiles

The configuration files for most of my set up. 

## zsh

Requires [zap-zsh](https://github.com/zap-zsh/zap).
This setup also requires [fzf](https://github.com/junegunn/fzf)

## XFCE4-Terminal

The font must be patched with webdev and codicons.

See [Nerd Fonts](https://github.com/ryanoasis/nerd-fonts).

Current favourite: [Hack - Nerd patched](https://github.com/ryanoasis/nerd-fonts/releases).

## tmux
Plug-in management with [tpm](https://github.com/tmux-plugins/tpm).

The [tmux-window-name plug-in](https://github.com/ofirgall/tmux-window-name#installation) requires an additional python package.

## Neovim

Use [neovim's app image](https://github.com/neovim/neovim/releases) for ease of use.

Plug-in management uses:
[Packer](https://github.com/wbthomason/packer.nvim#quickstart)
Check packer.lua for more requirements, some a listed below:
- [fd](https://github.com/sharkdp/fd#installation)
- [ripgrep](https://github.com/BurntSushi/ripgrep)
- build tools (e.g. gcc, cmake, etc.)

It uses [lsp-zero](https://github.com/VonHeikemen/lsp-zero.nvim) to setup most 
of the lsp-related plug-ins.

### nvim-jdtls

For Java the setup uses [nvim-jdtls](https://github.com/mfussenegger/nvim-jdtls) which has many dependencies.
Please check the configuration file `nvim/.config/nvim/ftplugin/java` and the plug-in documentation.

### SDKMAN

There is some SDKMAN(https://sdkman.io/) related configuration which will be handled more elegantly in the future.

For now, SDKMAN has to be installed using curl and bash (as they suggest).
