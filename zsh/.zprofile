# .zsh_profile

# User specific environment and startup programs
# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$PATH:$HOME/.local/bin"
fi

export GEM_HOME=$HOME/gems
export PATH=$PATH:$HOME/gems/bin
export MANPATH=$MANPATH:/home/rfguimaraes/.local/texlive/2024/texmf-dist/doc/man
export INFOPATH=$INFOPATH:/home/rfguimaraes/.local/texlive/2024/texmf-dist/doc/info
export PATH=$PATH:/home/rfguimaraes/.local/texlive/2024/bin/x86_64-linux

. "$HOME/.cargo/env"

setxkbmap -layout us -variant euro -option "compose:ralt"
